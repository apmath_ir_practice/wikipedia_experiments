package ru.spbu.apmath.pt;


import org.apache.commons.io.FileUtils;
import ru.spbu.apmath.pt.dump.WikiDumpConverter;
import ru.spbu.apmath.pt.mr.WikiStatExample;
import ru.spbu.apmath.pt.mr.WikiTextExtractor;

import java.io.File;

public class Check {
    public static void main(final String[] args) throws Exception {
        final String resources = Check.class.getResource("/").getFile();

        final String enwikiCutPath = resources + "/" + "enwiki_cut.xml";
        final String wikitextPath = resources + "/" + "wikitext";
        final String textPath = resources + "/" + "text";
        final String freqsPath = resources + "/" + "adv_freq";

        WikiDumpConverter.main(new String[] { enwikiCutPath, wikitextPath });
        WikiTextExtractor.main(new String[] { wikitextPath, textPath });
        WikiStatExample.main(new String[] { textPath, freqsPath });

        System.out.println(FileUtils.readFileToString(new File(freqsPath, "part-r-00000"), "utf-8"));
    }
}
