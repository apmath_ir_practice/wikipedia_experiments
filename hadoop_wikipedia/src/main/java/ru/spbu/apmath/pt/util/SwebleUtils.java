package ru.spbu.apmath.pt.util;

import org.sweble.wikitext.engine.CompiledPage;
import org.sweble.wikitext.engine.Compiler;
import org.sweble.wikitext.engine.PageId;
import org.sweble.wikitext.engine.PageTitle;
import org.sweble.wikitext.engine.config.WikiConfigurationInterface;

public class SwebleUtils {
    public static String getText(final Compiler compiler, final String wikiText, final String title) throws Exception {
        final WikiConfigurationInterface cfg = compiler.getWikiConfig();
        final PageTitle pageTitle = PageTitle.make(cfg, title);
        final PageId pageId = new PageId(pageTitle, -1);
        final CompiledPage page = compiler.postprocess(pageId, wikiText, null);

        final TextConverter p = new TextConverter(cfg, 80);
        return (String) p.go(page.getPage());
    }

}
