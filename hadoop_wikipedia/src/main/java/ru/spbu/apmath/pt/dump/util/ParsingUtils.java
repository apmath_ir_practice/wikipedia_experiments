package ru.spbu.apmath.pt.dump.util;

import com.google.common.collect.Sets;
import ru.spbu.apmath.pt.util.Pair;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.Charset;
import java.util.Set;


public class ParsingUtils {

    public static final Charset ENCODING = Charset.forName("utf-8");

    public static final String TEMPLATE_PREFIX = "Template:";
    public static final String CATEGORY_PREFIX = "Category:";
    public static final String ROOT_CATEGORY = "Category:Contents";

    private static final String STD_WIKI_PREFIX_URL = "http://en.wikipedia.org/wiki/";

    public static final String[] WIKI_NAMESPACES =  {
              "Template",  "Image", "Portal", "Wikipedia", "Mediawiki", "Help", "User", "File", "Book", "Talk", "Category", "User"
    };

    public static final Set<String> WIKI_NAMESPACES_SET = Sets.newHashSet(WIKI_NAMESPACES);

    @NotNull
    public static String wikipediaPageToWikiUrl(final String prefix, final String page) {
        final StringBuilder result = new StringBuilder(prefix);
        for (int i = 0; i < page.length(); i++) {
            if (page.charAt(i) == ' ') {
                result.append('_');
            } else {
                result.append(page.charAt(i));
            }
        }

        return result.toString();
    }

    @NotNull
    public static String wikipediaPageToWikiUrl(final String page) {
        return wikipediaPageToWikiUrl(STD_WIKI_PREFIX_URL, page);
    }

    @NotNull
    public static String normalizeWikiPageName(final String page) {
        return normalizeWikiPageName(new StringBuilder(page)).toString();
    }

    @NotNull
    public static StringBuilder normalizeWikiPageName(final StringBuilder page) {
        {
            int k;
            for (k = 0; k < page.length() && page.charAt(k) == ' '; k++) {
                //nothing
            }
            page.delete(0, k);

            for (k = page.length() - 1; k >= 0 && page.charAt(k) == ' '; k--) {
                //nothing
            }
            page.delete(k + 1, page.length());

            if (page.length() == 0) {
                return page;
            }
        }

        toUpperChar(page, 0);

        for (int i = 0; i < page.length(); i++) {
            if (page.charAt(i) == '_') {
                page.setCharAt(i, ' ');
            }
        }
        for (int i = 1; i < page.length(); i++) {
            if (page.charAt(i) == ':') {
                final StringBuilder namespace = new StringBuilder().append(page, 0, i);
                for (int k = 1; k < namespace.length(); k++) {
                    toLowerChar(namespace, k);
                }
                if (WIKI_NAMESPACES_SET.contains(namespace.toString())) {
                    for (int k = 1; k < i; k++) {
                        toLowerChar(page, k);
                    }
                    int k;
                    for (k = i + 1; k < page.length() && page.charAt(k) == ' '; k++) { }
                    page.delete(i + 1, k);

                    if (k < page.length()) {
                        toUpperChar(page, i + 1);
                    }

                    break;
                }
            }
        }


        return page;
    }

    @NotNull
    public static Pair<String, String> extractNamespace(final String page) {
        final int indexColon = page.indexOf(':');
        if (indexColon == -1 || indexColon == page.length() - 1) {
            return new Pair<String, String>(null, page);
        }

        final String beforeColon = page.substring(0, indexColon);
        if (WIKI_NAMESPACES_SET.contains(beforeColon)) {
            return new Pair<String, String>(beforeColon, page.substring(indexColon + 1));
        }

        return new Pair<String, String>(null, page);
    }

    private static StringBuilder toUpperChar(final StringBuilder builder, final int i) {
        final char ch = builder.charAt(i);
        if (Character.isLetter(ch)) {
            builder.setCharAt(i, Character.toUpperCase(ch));
        }

        return builder;
    }

    private static StringBuilder toLowerChar(final StringBuilder builder, final int i) {
        final char ch = builder.charAt(i);
        if (Character.isLetter(ch)) {
            builder.setCharAt(i, Character.toLowerCase(ch));
        }

        return builder;
    }

    private ParsingUtils() { }
}

