package ru.spbu.apmath.pt.dump.util;



public interface IWikiPageListener {
    void start();

    boolean isInterestPage(CharSequence id, CharSequence title);

    void processPage(CharSequence id, String title, CharSequence text);

    void end();
}
